import { validURL } from './validate'

export const validateValue = (rule, value, callback) => {
  const reg = /^[\u4E00-\u9FA5A-Za-z0-9]{1,15}$/ // 1-15个汉字、字母、数字
  if (!reg.test(value)) {
    callback(new Error('请输入1~15个字符'))
  } else {
    callback()
  }
}

export const validateNumber = (rule, value, callback) => {
  const reg = /^[0-9]{1,6}$/ // 1-15个汉字、字母、数字
  if (value === '') {
    callback(new Error('请输入设备码'));
  } else if (!reg.test(value)) {
    callback(new Error('请输入1~6位数字'))
  } else {
    callback()
  }
}
export const validatePhone = (rule, value, callback) => {
  const reg = /^1[3456789]\d{9}$/ // 1-15个汉字、字母、数字
  if (value === '') {
    callback(new Error('请输入手机号'))
  } else if (!reg.test(value)) {
    callback(new Error('手机号码有误，请重新填写'));
  } else {
    callback()
  }
}
export const validateOrgName = (rule, value, callback) => {
  const reg = /^[\u4e00-\u9fa5]+$/
  if (value === '') {
    callback(new Error('请输入机构名称'))
  } else if (!reg.test(value)) {
    callback(new Error('只支持中文输入'));
  } else {
    callback()
  }
}
export const validatestrLegal = (rule, value, callback) => {
  const reg = /^[\u4e00-\u9fa5]+$/
  if (value === '') {
    callback(new Error('请输入法人名称'))
  } else if (!reg.test(value)) {
    callback(new Error('只支持中文输入'));
  } else {
    callback()
  }
}
export const validatestrAddr = (rule, value, callback) => {
  const reg = /^[\u4E00-\u9FA5A-Za-z0-9]+$/
  if (value === '') {
    callback(new Error('请输入机构地址'))
  } else if (!reg.test(value)) {
    callback(new Error('只支持中文数字及英文字母输入'));
  } else {
    callback()
  }
}
export const validatestrRolename = (rule, value, callback) => {
  const reg = /^[\u4e00-\u9fa5]+$/
  if (value === '') {
    callback(new Error('请输入角色名称'))
  } else if (!reg.test(value)) {
    callback(new Error('只支持中文输入'));
  } else {
    callback()
  }
}
export const validatestrUsernum = (rule, value, callback) => {
  const reg = /^[A-Za-z0-9]+$/
  if (value === '') {
    callback(new Error('请输入用户账号'))
  } else if (!reg.test(value)) {
    callback(new Error('只支持英文和数字'));
  } else {
    callback()
  }
}
export const validatepwd = (rule, value, callback) => {
  const reg = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,}$/
  if (value === '') {
    callback(new Error('请输入用户密码'))
  } else if (!reg.test(value)) {
    callback(new Error('密码长度要大于6位，百由数字和字母组成'));
  } else {
    callback()
  }
}
export const validatestrVipNum = (rule, value, callback) => {
  const reg = /^[A-Za-z0-9]+$/
  if (!reg.test(value)) {
    callback(new Error('只支持英文和数字'));
  } else {
    callback()
  }
}
export const validatename = (rule, value, callback) => {
  const reg = /^[\u4e00-\u9fa5]+$/
  if (value === '') {
    callback(new Error('请输入用户姓名'))
  } else if (!reg.test(value)) {
    callback(new Error('只支持中文'));
  } else {
    callback()
  }
}
export const validatil = (rule, value, callback) => {
  const reg = /^[\u4E00-\u9FA5A-Za-z0-9_]+$/
  if (value === '') {
    callback(new Error('请输入职责'))
  } else if (!reg.test(value)) {
    callback(new Error('只支持中文、英文、数字包括下划线'));
  } else {
    callback()
  }
}

export const validateUrl = (rule, value, callback) => {
  if (value === '') {
    callback(new Error('URL地址不能为空'));
  } else if (!validURL(value)) {
    callback(new Error('URL地址格式错误，请重新填写'));
  } else {
    callback();
  }
}
