import Cookies from 'js-cookie'

const TokenKey = 'Authorization'

// token
export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
// 角色信息
export function setRoleInfo(roleInfo) {
  return Cookies.set('roleInfo', roleInfo)
}

export function getRoleInfo() {
  return Cookies.get('roleInfo')
}

export function removeRoleInfo() {
  return Cookies.remove('roleInfo')
}

// 当前账号名称
export function setName(name) {
  return Cookies.set('name', name)
}

export function getName() {
  return Cookies.get('name')
}

export function removeName() {
  return Cookies.remove('name')
}

/**
 *  存储用户信息
 *  @param Object userInfo
 */
export function setUserInfo(userInfo) {
  return Cookies.set('userInfo', userInfo)
}

export function getUserInfo(userInfo) {
  return Cookies.get('userInfo')
}

// 角色系统编号
export function setIRole(iRolesysnum) {
  return Cookies.set('iRolesysnum', iRolesysnum)
}

export function getIRole() {
  return Cookies.get('iRolesysnum')
}

export function removeRole() {
  return Cookies.remove('iRolesysnum')
}

// 机构编号
export function SET_IORG(iOrgsysnum) {
  return Cookies.set('iOrgsysnum', iOrgsysnum)
}

export function GET_IORG() {
  return Cookies.get('iOrgsysnum')
}

export function removeIORG() {
  return Cookies.remove('iOrgsysnum')
}

// 用户id
export function setUserID(userID) {
  return Cookies.set('userID', userID);
}

export function getUserID() {
  return Cookies.get('userID');
}

export function removeUserID() {
  return Cookies.remove('userID');
}
