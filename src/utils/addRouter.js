/**
 * 未解决 2020-0320
 * @param {*} routerlist
 */
export function addRouter(routerlist) {
  const router = [];

  try {
    routerlist.filter(e => {
      let e_new = {
        path: e.strPath,
        name: e.strName,
        hidden:
          e.strShow === 'false' ? false : e.strShow === 'true' ? true : null,
        component: () =>
          e.strComponent === 'Layout'
            ? import('@/layout')
            : import(`@/views${e.strComponent}`)
      };
      if (e.children) {
        const children = addRouter(e.children);
        // 保存权限
        e_new = { ...e_new, children: children };
      }
      if (e.strRedirect) {
        e_new = { ...e_new, redirect: e.strRedirect };
      }
      // if (e.iMenulevel === 0) {
      //   e_new = { ...e_new, hidden: true };
      // }
      if (e.icon !== '' && e.title !== '') {
        e_new = {
          ...e_new,
          meta: { title: e.strMeta.title, icon: e.strMeta.icon }
        };
      } else if (e.title !== '' && e.icon === '') {
        e_new = { ...e_new, meta: { title: e.strMeta.title }};
      }
      router.push(e_new);
    });
  } catch (error) {
    console.error(error);
    return [];
  }
  return router;
}
