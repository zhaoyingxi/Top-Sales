var pages = {
  data() {
    return {
      currentpage: 0,
      multipleSelection: []
    };
  },
  methods: {
    ResetCurrentPage() {
      const totalPage = Math.ceil((this.$data.total - 1) / this.$data.limit)
      const curPage = this.currentpage > totalPage ? totalPage : this.currentpage
      this.$data.pagenum = curPage < 1 ? 1 : curPage
    }
  }
};

export default pages;
