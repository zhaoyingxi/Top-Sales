import { getOrglist, getSysType, addBindApp } from '@/api/common';
import { addsearchPrograms } from '@/api/addTag';

const commonapi = {
  data() {
    return {
      optionsNum: [],
      optionType: [],
      optionTag: [],
      iDevTypes: [],
      addTableData: [],
      multipleSelection: []
    };
  },
  created() {
    this.getlist();
    this.getTagList();
  },
  methods: {
    getlist() {
      getOrglist()
        .then(res => {
          this.optionsNum = res.yhlist;
        })
        .catch(err => {
          console.log(err);
        });
    },
    getTagList() {
      getSysType({ typeInfo: [3, 4, 7] })
        .then(res => {
          this.optionType = res.syslist[4];
          this.optionTag = res.syslist[3];
          this.iDevTypes = res.syslist[7];
        })
        .catch(err => {
          console.log(err);
        });
    },
    // 新增页面弹窗中搜索
    addsearch(param) {
      if (this.addForm.iOrgsysnum instanceof Array) {
        this.addForm.iOrgsysnum = this.addForm.iOrgsysnum.splice(-1).toString();
      }
      addsearchPrograms(this.addForm).then(res => {
        this.addTableData = res.result;
        // this.dialogVisible1 = false;
      });
    },
    addsearchture() {
      if (this.multipleSelection.length > 0) {
        this.materialData = this.materialData.concat(this.multipleSelection);
        this.dialogVisible1 = false
      } else {
        this.$message({
          type: 'warning',
          message: '请先选择一条素材！'
        });
      }
    }
  }
};
export default commonapi;
