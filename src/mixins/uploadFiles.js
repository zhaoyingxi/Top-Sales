var mixin = {
  data() {
    return {
      baseImgRUL: process.env.VUE_APP_BASE_API2 + '/system/fileRequest',
      mixUploadUrl: ''
    }
  },
  methods: {
    beforeSkinUpload(file) {
      const isSkin = file.name.indexOf('skin') > 0
      if (!isSkin) {
        this.$message.error('上传文件必须是SKIN格式!')
      }
      return isSkin
    },
    handleSkinSuccess(res, file, form, strImg) {
      if (res.code === 200) {
        if (res.transCode === -1) {
          this.$message({
            type: 'error',
            message: res.message
          })
          return false
        }
        this.mixUploadUrl = res.result.result.replace(
          '/home/topsales/static',
          process.env.VUE_APP_BASE_API2
        )
        // this.$data[form][strImg] = URL.createObjectURL(file.raw)
        this.$data[form][strImg] = res.result.result.replace(
          '/home/topsales/static',
          process.env.VUE_APP_BASE_API2
        )
      }
    },
    handleExceed(files, fileList) {
      this.$message.warning(
        `一次只能上传1个文件，请先删除后再次上传`
      )
    }
  }
}

export default mixin
