import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

/* Layout */
import Layout from '@/layout';

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  // {
  //   path: '/redirect',
  //   component: Layout,
  //   hidden: true,
  //   children: [
  //     {
  //       path: '/redirect/:path(.*)',
  //       component: () => import('@/views/redirect/index')
  //     }
  //   ]
  // },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  // {
  //   path: '/auth-redirect',
  //   component: () => import('@/views/login/auth-redirect'),
  //   hidden: true
  // },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: '首页', icon: 'dashboard', affix: true }
      }
    ]
  }
];

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [

  {
    path: '/dataDevices',
    component: Layout,
    redirect: 'noRedirect',
    name: 'data-dictionary',
    meta: {
      title: '数据字典',
      icon: 'chart'
    },
    children: [
      {
        path: 'device-type',
        component: () => import('@/views/data-dictionary/device-type'),
        name: 'data-dictionary-device-type',
        meta: { title: '设备类型', noCache: true, icon: 'component' }
      }
    ]
  },
  {
    path: '/termina',
    component: Layout,
    redirect: 'noRedirect',
    name: 'termina',
    meta: {
      title: '终端管理',
      icon: 'chart'
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/termina/index'),
        name: 'termina-list',
        meta: { title: '终端清单', noCache: true }
      },
      {
        path: 'resolving',
        component: () => import('@/views/termina/resolving'),
        name: 'termina-resolving',
        meta: { title: '设备分辨率', noCache: true }
      },
      {
        path: 'publishlist',
        component: () => import('@/views/termina/publishlist'),
        name: 'termina-publish-list',
        meta: { title: '发布管理列表', noCache: true }
      },
      {
        path: 'publishAdd',
        component: () => import('@/views/termina/publish'),
        name: 'termina-publish-add',
        hidden: true,
        meta: { title: '发布管理', noCache: true }
      }
    ]
  },
  {
    path: '/product',
    component: Layout,
    name: 'Product',
    meta: { title: '产品类型', icon: 'example', noCache: true },
    children: [
      {
        path: 'index',
        component: () => import('@/views/product/index'),
        name: 'Product-index',
        meta: { title: '产品类型', noCache: true }
      },
      {
        path: 'list',
        component: () => import('@/views/product/productList'),
        name: 'Product-list',
        meta: { title: '产品列表', noCache: true }
      }
    ]
  },
  {
    path: '/programme',
    component: Layout,
    name: 'Programme',
    meta: { title: '节目单管理', icon: 'icon', noCache: true },
    children: [
      {
        path: 'index',
        component: () => import('@/views/programme/index'),
        name: 'Programme-index',
        meta: { title: '节目单管理', noCache: true }
      },
      {
        path: 'AddProgramme',
        hidden: true,
        component: () => import('@/views/programme/addProgramme'),
        name: 'Programme-AddProgramme',
        meta: { title: '新增节目单', noCache: true }
      },
      {
        path: 'editProgramme',
        hidden: true,
        component: () => import('@/views/programme/editProgramme'),
        name: 'Programme-editProgramme',
        meta: { title: '修改节目单', noCache: true }
      },
      {
        path: 'material',
        component: () => import('@/views/programme/material'),
        name: 'Product-material',
        meta: { title: '素材管理', noCache: true }
      },
      {
        path: 'tag',
        component: () => import('@/views/programme/material-tag'),
        name: 'Programme-material-tag',
        meta: { title: '素材标签管理', noCache: true }
      },
      {
        path: 'type',
        component: () => import('@/views/programme/material-type'),
        name: 'Product-material-type',
        meta: { title: '素材类型管理', noCache: true }
      }
    ]
  },
  // 应用部署
  {
    path: '/yystore',
    component: Layout,
    name: 'yystore',
    redirect: '/yystore',
    meta: { title: '应用商城', icon: 'tab' },
    children: [
      {
        path: 'yydeployment',
        component: () => import('@/views/yystore/yydeployment/index'),
        name: 'yydeployment',
        meta: { title: '应用部署', icon: 'tree' }
      },
      {
        path: 'appType',
        component: () => import('@/views/yystore/appType/index'),
        name: 'applicationType',
        meta: { title: '应用类型', icon: 'theme' }
      }
    ]
  },

  // 公告管理
  {
    path: '/notice',
    component: Layout,
    name: 'notice',
    redirect: '/notice',
    meta: { title: '公告管理', icon: 'tab' },
    children: [
      {
        path: 'index',
        component: () => import('@/views/notice/notices/index'),
        name: 'notices',
        meta: { title: '公告管理', icon: 'documentation' }
      }
    ]
  },

  // 活动中心
  {
    path: '/activity',
    component: Layout,
    name: 'activity',
    redirect: '/activity',
    meta: { title: '活动中心', icon: 'tab' },
    children: [
      {
        path: 'index',
        component: () => import('@/views/activityPage/index'),
        name: 'index',
        meta: { title: '活动管理', icon: 'dashboard' }
      }
    ]
  },
  {
    path: '/system',
    component: Layout,
    meta: { title: '系统中心', icon: 'el-icon-s-tools', noCache: true },
    children: [
      {
        path: 'orglist',
        component: () => import('@/views/system/orglist'),
        name: 'orglist',
        meta: { title: '机构管理', noCache: true, icon: 'chart' }
      }
    ]
  },
  {
    path: '/authority',
    component: Layout,
    meta: { title: '权限中心', icon: 'drag', noCache: true },
    children: [
      {
        path: 'applist',
        component: () => import('@/views/system/applist'),
        name: 'applist',
        meta: { title: '终端角色管理', noCache: true }
      },
      {
        path: 'pclist',
        component: () => import('@/views/system/pclist'),
        name: 'pclist',
        meta: { title: '运管角色管理', noCache: true }
      }
    ]
  },
  {
    path: '/user',
    component: Layout,
    meta: { title: '用户中心', icon: 'peoples', noCache: true },
    children: [
      {
        path: 'userlist',
        component: () => import('@/views/system/userlist'),
        name: 'userlist',
        meta: { title: '运管用户管理', noCache: true }
      },
      {
        path: 'appuser',
        component: () => import('@/views/system/appuser'),
        name: 'appuser',
        meta: { title: '终端用户管理', noCache: true }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
];

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
  });

const router = createRouter();

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;
