import request from '@/utils/request';
import qs from 'qs';

/**
 * 机构回显
 * @param {id}
 */

export function getParentNumArr(listQuery) {
  return request({
    url: '/device/getLastorgnum',
    method: 'post',
    data: qs.stringify(listQuery)
  });
}

/**
 * 显示所有机构列表信息
 * @param
 */
export function getOrglist() {
  return request({
    url: '/device/getAllOrgInfo',
    method: 'post'
  });
}

/**
 *  数据字典 新增设备
 *  @param refDevice(数组)
 *  @param strDesc（DeviceType）
 *  @param iType（7）
 */
export function getSysType(query) {
  return request({
    url: '/system/getSystemType',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  });
}
