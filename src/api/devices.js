import request from '@/utils/request';
import qs from 'qs';
/**
 * 终端管理  根据设备系统编号获取设备已绑定应用
 * @param {id} ID
 */
export function getAppByDevId(id) {
  return request({
    url: '/device/selectappbydevicenum',
    method: 'post',
    data: qs.stringify(id)
  });
}

/**
 * 终端管理  根据机构号获取所有应用
 * @param {id} ID
 */
export function getAppByorgNum(id) {
  return request({
    url: '/device/selectappbyorgnum',
    method: 'post',
    data: qs.stringify(id)
  });
}
/**
 * 终端管理  根据机构号获取所有产品
 * @param {id} ID
 */
export function getprctByorgNum(id) {
  return request({
    url: '/device/selectproductbyorgnum',
    method: 'post',
    data: qs.stringify(id)
  });
}
/**
 * 终端管理  根据设备系统编号获取设备已绑定节目单
 * @param {id} ID
 */
export function getProgramByDevId(id) {
  return request({
    url: '/device/selectprogrambydevicenum',
    method: 'post',
    data: qs.stringify(id)
  });
}
/**
 * 终端管理  根据设备系统编号获取设备已绑定产品
 * @param {id} ID
 */
export function getPructByDevId(id) {
  return request({
    url: '/device/selectpruductbydevicenum',
    method: 'post',
    data: qs.stringify(id)
  });
}

/**
 * 终端管理  根据机构号获取所有节目单
 * @param {id} ID
 */
export function getProgmByorgNum(id) {
  return request({
    url: '/device/selectprogrambyorgnum',
    method: 'post',
    data: qs.stringify(id)
  });
}
