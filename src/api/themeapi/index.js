import request from '@/utils/request';
import qs from 'qs'

/**
 * 皮肤列表
 * @param(iDevicetype) 设备类型 1
 *
 */
export function fetchThemeData(query) {
  return request({
    url: '/themeInfo/list',
    method: 'post',
    data: qs.stringify(query)
  })
}
/**
 * 所属设备列表
 * @param(type) 设备类型 2
 *
 */
export function fetchDeviceInfo(query) {
  return request({
    url: '/themeInfo/queryDeviceInfo',
    method: 'post',
    data: qs.stringify(query)
  });
}

/**
 * 添加皮肤
 * @param(Object)
 *
 */
export function confirmAddTheme(query) {
  return request({
    url: '/themeInfo/addTheme',
    method: 'post',
    data: qs.stringify(query)
  });
}
/**
 * 删除皮肤
 * @param(Object)
 *
 */
export function deleteRowTheme(query) {
  return request({
    url: '/themeInfo/deleteTheme',
    method: 'post',
    data: qs.stringify(query)
  });
}
/**
 * 修改皮肤
 * @param(Object)
 *
 */
export function updateRowTheme(query) {
  return request({
    url: '/themeInfo/updateTheme',
    method: 'post',
    data: qs.stringify(query)
  });
}
