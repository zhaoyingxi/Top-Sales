import request from '@/utils/request'
import qs from 'qs'

/**
 * 首页接口
 * **/
// 已部署数量、 用户数量、机构数量
export function getIndexDatalin(query) {
  return request({
    url: '/indexPage/ry_getIndexData',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

// 获取折线图当前设备状态
export function getEchartData(query) {
  return request({
    url: '/indexPage/ry_getEchartData',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

// 设备种类数据
export function getDevicetype(query) {
  return request({
    url: '/indexPage/ry_getDeviceInfos',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

// 获取饼状图数据(应用)
export function getAppInfos(query) {
  return request({
    url: '/indexPage/ry_getAppInfos',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

/**
 * 应用操作模块接口
 */
// 获取应用部署列表,支持分页
export function getdeploymentList(query) {
  return request({
    url: '/appInfo/ry_selectAllAppInfo',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

// 获取应用类型select  eq:数据字典接口
export function getselectList(query) {
  return request({
    url: '/system/getSystemType',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

// 删除应用
export function deleteInfo(query) {
  return request({
    url: '/appInfo/ry_deleteAppInfo',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

// 批量删除应用
export function deleteAllInfo(query) {
  return request({
    url: '/appInfo/ry_deleteAppInfoList',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

// 查看应用详情
export function detailTable(query) {
  return request({
    url: '/appInfo/ry_selectByAppnum',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

// (修改/详情)通过应用编号查询已经关联机构的应用信息
export function detailrelevance(query) {
  return request({
    url: '/appInfo/ry_selectRefApporgByAppid', // 树回显接口
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

// 添加应用
export function addInfo(query) {
  return request({
    url: '/appInfo/ry_insertAppInfo',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

// 获取信息
export function getInfo(query) {
  return request({
    url: '/appInfo/ry_getInfo',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

// 根据应用类型查询基础应用
export function selectByType(query) {
  return request({
    url: '/appInfo/ry_selectByType',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

// 修改应用
export function updateAppInfo(query) {
  return request({
    url: '/appInfo/ry_updateAppInfo',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

/**
 * 公告管理模块接口
 */
// 公告总列表
export function getNotice(query) {
  return request({
    url: '/notice/ry_getINoticeInfo',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

// 删除公告
export function DeleteNotice(query) {
  return request({
    url: '/notice/ry_deleteOrgInfo',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

// 批量删除公告
export function deleteAllnotice(query) {
  return request({
    url: '/notice/ry_deleteINoticeInfoByNoticenumList',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

// 公告详情
export function DetailNotice(query) {
  return request({
    url: '/notice/ry_getNoticeInfoByNoticenum',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

// 机构获取全部
export function getOrglist() {
  return request({
    url: '/device/getAllOrgInfo',
    method: 'post'
  })
}

// 机构获取权限(本身以及下属)
export function getOrglists(query) {
  return request({
    url: '/device/getAllOrgInfos',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

// 添加公告
export function addNotice(query) {
  return request({
    url: '/notice/ry_insertOrginfo',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

// 修改公告
export function updateNotice(query) {
  return request({
    url: 'notice/ry_updateOrginfo',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

export function addMaterApp(query) {
  return request({
    url: '/appInfo/addRefAppList',
    method: 'post',
    data: qs.stringify(query)
  });
}
export function fetchMaByApplist(query) {
  return request({
    url: '/appInfo/selectByMaterial',
    method: 'post',
    data: qs.stringify(query)
  });
}
