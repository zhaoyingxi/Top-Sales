import request from '@/utils/request'
import qs from 'qs'

// 获取列表
export function getList(data) {
  return request({
    url: '/programList/ry_getProgramList',
    method: 'post',
    data
  })
}

// 终端新增
export function addDeviceInfo(query) {
  return request({
    url: '/device/addDeviceInfo',
    method: 'post',
    data: qs.stringify(query)
  })
}

/**
 * 终端管理 删除
 * @param {id} ID
 */
export function deleteDeviceInfo(id) {
  return request({
    url: '/device/deleteDeviceInfo',
    method: 'post',
    data: qs.stringify(id)
  })
}

// 参数类型
export function getDeviceType(query) {
  return request({
    url: '/device/getDeviceType',
    method: 'post',
    data: qs.stringify(query)
  })
}

// 终端信息
export function getDeviceInfoByID(data) {
  return request({
    url: '/device/getDeviceInfoByID',
    method: 'post',
    data
  })
}

/**
 * 显示所有机构列表信息
 * @param
 */
export function getOrglist() {
  return request({
    url: '/device/getAllOrgInfo',
    method: 'post'
  })
}
// 机构获取权限(本身以及下属)
export function getOrglists(iOrgnum) {
  return request({
    url: '/device/getAllOrgInfos',
    method: 'post',
    data: qs.stringify(iOrgnum)
  })
}
/**
 * 所在机构列表
 * 获取列表
 * @param {page} 页数
 */
export function getDeviceList(page) {
  return request({
    url: '/device/getAllpageDeviceList',
    method: 'post',
    data: qs.stringify(page)
  })
}

/**
 * 默认应用
 */
export function getDetaultapp(query) {
  return request({
    url: '/device/getjcapp',
    method: 'post',
    data: qs.stringify(query)
  })
}

/**
 * 修改
 */
export function updateDevData(query) {
  return request({
    url: '/device/updateDeviceInfo',
    method: 'post',
    data: qs.stringify(query)
  })
}

/**
 * 发布节目单
 * @param
 */
export function ReleasePrograms(query) {
  return request({
    url: '/device/fabuDeviceInfo',
    method: 'post',
    data: qs.stringify(query)
  })
}

/**
 * 新增节目单
 * @param
 */
export function addPrograms(query) {
  return request({
    url: '/programList/ry_addProgramList',
    method: 'post',
    data: qs.stringify(query, { indices: false, allowDots: true })
  })
}

/**
 * (节目单)紧急播放获取设备列表
 * @param
 */
export function getDevice(query) {
  return request({
    url: '/programList/ry_getDeviceByOrgsysnum',
    method: 'post',
    data: qs.stringify(query, { indices: false, allowDots: true })
  })
}

/**
 *lh 发布节目单
 * **/
export function udpRelease(query) {
  return request({
    url: '/programList/ry_udpRelease',
    method: 'post',
    data: qs.stringify(query, { indices: false, allowDots: true })
  })
}

/**
 * 删除单条节目单
 * */
export function deleteProgram(query) {
  return request({
    url: '/programList/ry_deleteProgramList',
    method: 'post',
    data: qs.stringify(query, { arrayFormat: 'indices', allowDots: true })
  })
}

/**
 * 批量删除节目单
 * **/
export function deleteAllProgramList(query) {
  return request({
    url: '/programList/ry_deleteProgramListList',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

/**
 * 新增节目单页面搜索
 * */
export function addsearchPrograms(query) {
  return request({
    url: '/programList/ry_getMaterialInfoByOrgsysnum',
    method: 'post',
    data: qs.stringify(query, { arrayFormat: 'indices', allowDots: true })
  })
}

/**
 * 详情节目单
 * */
export function detailPrograms(query) {
  return request({
    url: '/programList/ry_getProgramListByProgramsysnum',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}
/**
 * 详情里根据节目单编号查询素材
 * */
export function detailsucai(query) {
  return request({
    url: '/programList/ry_getMaterialInfoByProgramsysnum',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}
/**
 * 修改节目单
 * */
export function changePrograms(query) {
  return request({
    url: '/programList/ry_udpProgramList',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}
/**
 * 数据字典列表
 * iType 参数类型
 * pagenum 分页数
 */
export function getSysList(query) {
  return request({
    url: '/system/getAllSysparam',
    method: 'post',
    data: qs.stringify(query)
  })
}

/**
 * 数据字典 删除
 * id
 * strCode 参数码
 */
export function deleteDev(query) {
  return request({
    url: '/system/deleteDeviceType',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

/**
 * 数据字典 获取基础应用
 *  iApptype(应用类型) -1 （固定值）
 */
export function getBaseApp(query) {
  return request({
    url: '/system/getByType',
    method: 'post',
    data: qs.stringify(query)
  })
}

/**
 *  数据字典 新增设备
 *  refDevice(数组)
 *  strDesc（DeviceType）
 *  iType（7）
 */
export function addDevType(query) {
  return request({
    url: '/system/addDevicetype',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

/**
 *  数据字典 新增设备
 *  refDevice(数组)
 *  strDesc（DeviceType）
 *  iType（7）
 */
export function findDevicetype(query) {
  return request({
    url: '/system/findDevicetype',
    method: 'post',
    data: qs.stringify(query)
  })
}

/**
 *  数据字典 新增设备
 *  refDevice(数组)
 *  strDesc（DeviceType）
 *  iType（7）
 */
export function getSysType(query) {
  return request({
    url: '/system/getSystemType',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  })
}

/**
 *  数据字典 修改设备类型
 *  refDevice(数组)
 *  strDesc（DeviceType）
 *  iType（7）
 */
export function modifyDevType(query) {
  return request({
    url: '/system/updateDevicetype',
    method: 'post',
    data: qs.stringify(query, { allowDots: true })
  })
}

/**
 *  数据字典 新增
 *  parm(数组)
 *  strDesc（DeviceType）
 *  iType（7）
 */
export function addTag(query) {
  return request({
    url: '/system/addSystemType',
    method: 'post',
    data: qs.stringify(query)
  })
}

/**
 *  数据字典 删除
 *  @param id
 */
export function deleteSysParam(id) {
  return request({
    url: '/system/deleteSysParam',
    method: 'post',
    data: qs.stringify(id, { indices: false })
  })
}

/**
 *  数据字典 更新
 *  @param listQuery
 */
export function updateSys(listQuery) {
  return request({
    url: '/system/updateSystemType',
    method: 'post',
    data: qs.stringify(listQuery)
  })
}

/**
 * 节目单管理
 * 获取列表
 * @param {pagenum} 分页
 */
export function getProgramList(query) {
  return request({
    url: '/programList/ry_getProgramList',
    method: 'post',
    data: qs.stringify(query)
  })
}

// /**
//  *  节目单 列表
//  *  @param listQuery
//  */
// export function getProgramList(listQuery) {
//   return request({
//     url: 'programList/ry_getProgramList',
//     method: 'post',
//     data: qs.stringify(listQuery)
//   });
// }

/**
 *  素材管理 列表
 *  @param listQuery
 */
export function getMaterList(listQuery) {
  return request({
    url: '/material/getSelectTypeMaterInfo',
    method: 'post',
    data: qs.stringify(listQuery)
  })
}

/**
 *  素材管理 新增
 *  @param listQuery
 */
export function addMaterial(listQuery) {
  return request({
    url: '/material/addMaterialInfo',
    method: 'post',
    data: qs.stringify(listQuery)
  })
}

/**
 *  素材管理 删除
 *  @param idInfo Array[]
 *  @param id string
 */
export function deleteMaterial(listQuery) {
  return request({
    url: '/material/detMaterialInfo',
    method: 'post',
    data: qs.stringify(listQuery, { indices: false })
  })
}

/**
 *  活动管理 列表
 *  @param listQuery
 */
export function getActList(listQuery) {
  return request({
    url: '/activity/getSelectTypeActiveInfo',
    method: 'post',
    data: qs.stringify(listQuery, { arrayFormat: 'brackets' })
  })
}

/**
 *  活动管理 删除
 *  @param idInfo []
 */
export function deleteActRow(idInfo) {
  return request({
    url: '/activity/deleteActiveInfo',
    method: 'post',
    data: qs.stringify(idInfo, { indices: false })
  })
}

/**
 *  活动管理 新增
 *  @param idInfo []
 */
export function addActInfo(listQuery) {
  return request({
    url: '/activity/addActiveInfo',
    method: 'post',
    data: qs.stringify(listQuery)
  })
}

/**
 *  活动管理 新增 /activity/updateActiveInfo
 *  @param idInfo []
 */
export function updateActInfo(listQuery) {
  return request({
    url: '/activity/updateActiveInfo',
    method: 'post',
    data: qs.stringify(listQuery)
  })
}

/**
 *  绑定应用
 *  @param iAppnum 应用编号(系统)
 *  @param iMaterialnum 素材编号(系统)
 */
export function addBindApp(listQuery) {
  return request({
    url: '/appInfo/addRefApp',
    method: 'post',
    data: qs.stringify(listQuery)
  })
}

export function getproductInfo(pageNum) {
  return request({
    url: '/productInfo/list',
    method: 'post',
    data: qs.stringify(pageNum)
  })
}

export function deleteproductInfo(iProductnum) {
  return request({
    url: '/productInfo/deleteProduct',
    method: 'post',
    data: qs.stringify(iProductnum)
  })
}
export function addProduct(productInfo) {
  return request({
    url: 'productInfo/addProduct',
    method: 'post',
    data: qs.stringify(productInfo)
  })
}
export function updateProduct(productInfo) {
  return request({
    url: 'productInfo/updateProduct',
    method: 'post',
    data: qs.stringify(productInfo)
  })
}
