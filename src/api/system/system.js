import request from '@/utils/request'
import qs from 'qs'
export function getList(rows, strRolenum) {
  return request({
    url: '/roleInfo/getRoleSelect',
    method: 'post',
    data: qs.stringify(rows, strRolenum)
  })
}

export function listRoleNum(iType) {
  return request({
    url: '/roleInfo/listRoleNum',
    method: 'post',
    data: qs.stringify(iType)
  })
}
export function deleteRole(id) {
  return request({
    url: '/roleInfo/delRoleInfo',
    method: 'post',
    data: qs.stringify(id)
  })
}
export function getAppRoleList(rows, strRolenum) {
  return request({
    url: '/roleInfo/getRoleSelect',
    method: 'post',
    data: qs.stringify(rows, strRolenum)
  })
}
export function getOrgList(rows) {
  return request({
    url: '/orgInfo/getOrgSelect',
    method: 'post',
    data: qs.stringify(rows)
  })
}
export function deleteOrg(id, iOrgsysnum) {
  return request({
    url: '/orgInfo/delOrgInfo',
    method: 'post',
    data: qs.stringify(id, iOrgsysnum)
  })
}
export function selectAppUserInfo(rows, iUsertype, strPhone, iOrgsysnum) {
  return request({
    url: '/userInfo/getUserSelect',
    method: 'post',
    data: qs.stringify(rows, iUsertype, strPhone, iOrgsysnum)
  })
}
export function listRoleAuth() {
  return request({
    url: '/roleInfo/listRoleAuth',
    method: 'post',
    data: qs.stringify()
  })
}
export function addRoleInfo(roleInfo) {
  return request({
    url: '/roleInfo/addRoleInfo',
    method: 'post',
    data: qs.stringify(roleInfo)
  })
}
export function updRoleInfo(roleInfo) {
  return request({
    url: '/roleInfo/updRoleInfo',
    method: 'post',
    data: qs.stringify(roleInfo)
  })
}

export function showRoleAuth(iRolesysnum) {
  return request({
    url: '/roleInfo/showRoleAuth',
    method: 'post',
    data: qs.stringify(iRolesysnum, { indices: false })
  })
}

export function getAppRoleInfo(pageNum, roleName) {
  return request({
    url: '/roleInfo/queryUserInfo',
    method: 'post',
    data: qs.stringify(pageNum, roleName)
  })
}
export function getchanpin(pageNum, roleName) {
  return request({
    url: '/roleInfo/queryUserInfo',
    method: 'post',
    data: qs.stringify(pageNum, roleName)
  })
}
export function delUserInfo(id) {
  return request({
    url: '/userInfo/delUserInfo',
    method: 'post',
    data: qs.stringify(id)
  })
}

export function queryProduct(iOrgsysnum) {
  return request({
    url: '/roleInfo/queryProduct',
    method: 'post',
    data: qs.stringify(iOrgsysnum)
  })
}

export function getOrgListByelvel(iOrglevel) {
  return request({
    url: '/orgInfo/getOrgList',
    method: 'post',
    data: qs.stringify(iOrglevel)
  })
}
export function addOrgInfo(orgInfo) {
  return request({
    url: '/orgInfo/addOrgInfo',
    method: 'post',
    data: qs.stringify(orgInfo)
  })
}
export function queryNextOrg(iOrglevel) {
  return request({
    url: '/orgInfo/queryNextOrg',
    method: 'post',
    data: qs.stringify(iOrglevel)
  })
}
export function updOrgInfo(orgInfo) {
  return request({
    url: '/orgInfo/updOrgInfo',
    method: 'post',
    data: qs.stringify(orgInfo, { indices: false })
  });
}
export function queryRoleByTerminal(iType) {
  return request({
    url: '/userInfo/queryRoleByTerminal',
    method: 'post',
    data: qs.stringify(iType)
  })
}
export function queryUserRole(iRolesysnum) {
  return request({
    url: '/roleInfo/queryUserRole',
    method: 'post',
    data: qs.stringify(iRolesysnum)
  })
}
export function addUserInfo(userInfo) {
  return request({
    url: '/userInfo/addUserInfo',
    method: 'post',
    data: qs.stringify(userInfo, { indices: false })
  });
}

export function listUserRole(iUsersysnum) {
  return request({
    url: '/roleInfo/listUserRole',
    method: 'post',
    data: qs.stringify(iUsersysnum)
  })
}
export function updUserInfo(userInfo) {
  return request({
    url: '/userInfo/updUserInfo',
    method: 'post',
    data: qs.stringify(userInfo, { indices: false })
  })
}
export function getProductTypeAndName(iRolesysnum) {
  return request({
    url: 'device/getProductTypeAndName',
    method: 'post',
    data: qs.stringify(iRolesysnum)
  })
}
export function listRoleProduct(iRolesysnum) {
  return request({
    url: '/roleInfo/listRoleProduct',
    method: 'post',
    data: qs.stringify(iRolesysnum)
  })
}

export function queryNextbyOrgsysnum(iOrgsysnum, iOrglevel) {
  return request({
    url: '/orgInfo/queryNextOrgName',
    method: 'post',
    data: qs.stringify(iOrgsysnum, iOrglevel)
  })
}
