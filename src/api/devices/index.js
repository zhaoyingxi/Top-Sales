import request from '@/utils/request'
import qs from 'qs'

/**
 * 默认应用
 * @param(iDevicetype) 设备类型 1
 *
 */
export function getDetaultapp(query) {
  return request({
    url: '/device/getjcapp',
    method: 'post',
    data: qs.stringify(query)
  })
}

/**
 * 配置终端基础信息
 * @param(iDevicetype) 设备类型 1
 *
 */
export function getDetaultapp1(query) {
  return request({
    url: '/device/updatesDeviceInfo',
    method: 'post',
    data: qs.stringify(query)
  })
}

/**
 * 配置终端基础信息
 * @param
 */
export function updatesDevData(query) {
  return request({
    url: '/device/updatesDeviceInfo',
    method: 'post',
    data: qs.stringify(query)
  })
}

/**
 * 所在机构列表
 * 获取列表
 * @param {page} 页数
 */
export function getDeviceData(page) {
  return request({
    url: '/device/getAllpageDeviceLists',
    method: 'post',
    data: qs.stringify(page)
  })
}
/**
 * 根据两个节目单系统编号查询两个节目单播放是否冲突
 *
 * @param {iProgramsysnum1} 节目单系统编号1
 * @param {iProgramsysnum2} 节目单系统编号2
 *
 */
export function uniqueIPrograms(listQuery) {
  return request({
    url: '/device/selectByprogramSysnum',
    method: 'post',
    data: qs.stringify(listQuery)
  });
}
export function beforeList(listQuery) {
  return request({
    url: '/device/getAllpageDeviceLists',
    method: 'post',
    data: qs.stringify(listQuery)
  });
}
export function mqsend(listQuery) {
  return request({
    url: '/device/mqsend',
    method: 'post',
    data: qs.stringify(listQuery)
  });
}
