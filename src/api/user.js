import request from '@/utils/request'
import qs from 'qs'

export function login(query) {
  return request({
    url: '/userInfo/login',
    method: 'post',
    data: qs.stringify(query)
  })
}

export function getInfo() {
  return request({
    url: '/userInfo/getUserInfo',
    method: 'post'
  });
}

export function logout() {
  return request({
    url: '/userInfo/logout',
    method: 'post'
  });
}

export function getMenu(iRolesysnum) {
  return request({
    url: '/userInfo/getMenuRole',
    method: 'post',
    data: qs.stringify(iRolesysnum)
  })
}
