import request from '@/utils/request'
import qs from 'qs'

/**
 * 修改素材
 * @param Object
 *
 */
export function updateMaterInfo(query) {
  return request({
    url: '/material/upMaterialInfo',
    method: 'post',
    data: qs.stringify(query)
  })
}
