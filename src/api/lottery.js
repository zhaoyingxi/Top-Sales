import request from '@/utils/request';
import qs from 'qs'

/**
 * 抽奖
 * 查询接口
 */
export function fetchList(query) {
  return request({
    url: '/lottery/findLotteryInfo',
    method: 'post',
    data: qs.stringify(query)
  });
}

/**
 * 删除抽奖接口
 */
export function deleteRow(query) {
  return request({
    url: '/lottery/deleteLotteryInfo',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  });
}

/**
 * 新增抽奖接口
 * @param(Array) idInfo
 */
export function addRowData(query) {
  return request({
    url: '/lottery/addLotteryInfo',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  });
}

/**
 * 修改接口
 */
export function updataRow(query) {
  return request({
    url: '/lottery/updateLotteryInfo',
    method: 'post',
    data: qs.stringify(query, { indices: false })
  });
}
/**
 * 获取奖项接口  lottery
 */
export function getAwardlist(query) {
  return request({
    url: '/lottery/getAwardInfo',
    method: 'post',
    data: qs.stringify(query)
  });
}
