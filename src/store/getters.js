const getters = {
  sidebar: state => state.app.sidebar,
  size: state => state.app.size,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  token: state => state.user.token,
  roleInfo: state => state.user.roleInfo,
  userInfo: state => state.user.userInfo,
  iOrgsysnum: state => state.user.iOrgsysnum,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  introduction: state => state.user.introduction,
  roles: state => state.user.roles,
  permission_routes: state => state.permission.routes,
  errorLogs: state => state.errorLog.logs,
  RouterList: state => state.user.RouterList,
  iRolesysnum: state => state.user.iRolesysnum,
  userID: state => state.user.userId
};
export default getters
