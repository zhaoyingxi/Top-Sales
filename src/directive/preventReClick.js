import Vue from 'vue'
const preventReClick = Vue.directive('preventReClick', {
  inserted(el, binding) {
    el.addEventListener('click', () => {
      if (!el.disabled) {
        el.disabled = true
        el.classList.add('is-disabled')
        setTimeout(_ => {
          el.disabled = false
          el.classList.remove('is-disabled')
        }, binding.value || 3000)
      }
    })
  }
})
export default preventReClick
